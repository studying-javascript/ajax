function getUsers(cb) {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/users");
  xhr.send();

  xhr.addEventListener("load", () => {
    cb(JSON.parse(xhr.responseText));
  });
}

function showUsers(users) {
  const div = document.querySelector("div");
  let fragment = document.createDocumentFragment();
  users.forEach(el => {
    const card = document.createElement("div");
    card.classList.add("card");
    card.id = el.id;

    const cardBody = document.createElement("div");
    cardBody.classList.add("card-body");

    const cardTitle = document.createElement("h5");
    cardTitle.classList.add("card-title");
    cardTitle.textContent = el.name;

    cardBody.appendChild(cardTitle);
    card.appendChild(cardBody);

    fragment.appendChild(card);
  });
  div.appendChild(fragment);
}

(function() {
  const div = document.querySelector("div");
  getUsers(showUsers);
})();
